#! /usr/bin/env python3

import json

#myObject = {"alpha":42, "bravo":23, "charly":15}
myObject = {"alpha":42, "bravo":True , "charly":[15, 23], "delta":{"echo":12, "fox":[23, 15]}}

# json.dumps(...) generates a string
print(json.dumps(myObject))

# json.dump(...) writes the data to a file
with open("myObject.json", "w") as jsonFile:
	json.dump(myObject, jsonFile)

