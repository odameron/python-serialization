#! /usr/bin/env python3

import pickle

#myObject = {"alpha":42, "bravo":23, "charly":15}
myObject = {"alpha":42, "bravo":True , "charly":[15, 23], "delta":{"echo":12, "fox":[23, 15]}}

with open("myObject.pkl", "wb") as pklFile:
	pickle.dump(myObject, pklFile)

