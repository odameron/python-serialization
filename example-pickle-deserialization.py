#! /usr/bin/env python3

import pickle

myObject = {} # optional initialization

with open("myObject.pkl", "rb") as pklFile:
	myObject = pickle.load(pklFile)

print(myObject)
