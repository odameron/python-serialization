# python-serialization

A brief tutorial on serializing objects in python (i.e. representing them by a string for being able to store and restore them).

See [python-serialization.md](python-serialization.md)


# Todo

- [x] provide code examples
    - [x] pickle
    - [x] json
- [ ] compare the size of pickle (compressed) and uncompressed json for large objects

