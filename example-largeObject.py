#! /usr/bin/env python3

import json
import pickle

myObject = {}
for i in range(1000):
	myObject[str(i)] = i

with open("largeObject.pkl", "wb") as pklFile:
	pickle.dump(myObject, pklFile)

with open("largeObject.json", "w") as jsonFile:
	json.dump(myObject, jsonFile)

