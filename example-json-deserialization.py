#! /usr/bin/env python3

import json

myObject = {} # optional initialization

# json.loads(jsonString) generates an object from a string
print(json.loads('{"alpha": 42, "bravo": 23, "charly": 15}'))

# json.loads(jsonString) generates an object from a 
with open("myObject.json", "r") as jsonFile:
	myObject = json.load(jsonFile)
	print(myObject)

