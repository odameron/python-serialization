Serializing objects in python

# Definition

**Serializing an object** consists in univocally generating a string representation of an object so that:

- the string can be archived (e.g. saved in a file);
- the object can be recreated from the string.

Serialization is particularly useful for saving intermediary results so that a study can be resumed after interruption without having to start over from the begining.

Typical solutions include using the dedicated python library [pickle](https://docs.python.org/3/library/pickle.html), which should be prefered over the more primitive marshall library, or generating a json representation.
In any case, writing your own functions for dumping an object into a file and recreating it is probably a very bad idea, unless you have a compelling reason to do so!

See [https://docs.python.org/3/library/pickle.html#comparison-with-json](https://docs.python.org/3/library/pickle.html#comparison-with-json) for a comparison of pickle and json.

Notice that by default, json is just a string representation, which can become large for large object (which are typically the cases you need serialization most), whereas pickle uses a smaller binary representation (according to [https://docs.python.org/3/library/pickle.html#data-stream-format](https://docs.python.org/3/library/pickle.html#data-stream-format), the pickle binary format can be compressed for optimal size).


# Serialization with pickle

## Serialize objects

```python
import pickle

#myObject = {"alpha":42, "bravo":23, "charly":15}
myObject = {"alpha":42, "bravo":True , "charly":[15, 23], "delta":{"echo":12, "fox":[23, 15]}}

with open("myObject.pkl", "wb") as pklFile:
	pickle.dump(myObject, pklFile)
```

## De-serialize


```python
import pickle

myObject = {} # optional initialization

with open("myObject.pkl", "rb") as pklFile:
	myObject = pickle.load(pklFile)
```



# Serialisation with json

## Serialize objects

```python
import json

#myObject = {"alpha":42, "bravo":23, "charly":15}
myObject = {"alpha":42, "bravo":True , "charly":[15, 23], "delta":{"echo":12, "fox":[23, 15]}}

# json.dumps(...) generates a string
print(json.dumps(myObject))

# json.dump(...) writes the data to a file
with open("myObject.json", "w") as jsonFile:
	json.dump(myObject, jsonFile)
```

## De-serialize


```python
import json

myObject = {} # optional initialization

# json.loads(jsonString) generates an object from a string
print(json.loads('{"alpha": 42, "bravo": 23, "charly": 15}'))

# json.loads(jsonString) generates an object from a 
with open("myObject.json", "r") as jsonFile:
	myObject = json.load(jsonFile)
	print(myObject)
```
